SerialProtect
=============

Provides an enhanced ObjectInputStream that can apply policies to prevent certain types
of malicious or unintentional data in serialized Java data.


Example
-------

Here's a silly example that blacklists `java.lang.Long` objects from being read:
```java
InputPolicy policy = new InputPolicy.Builder()
	.whitelistPackage( "java.lang" )
	.blacklist( Long.class )
	.build();
ObjectInputStream in = new ProtectedObjectInputStream( policy, source );
```
If a `Long` is encountered in the stream, the operation will fail with a 
`DeniedClassException`.



Status, direction and the future
--------------------------------

The intention of this library is to provide as much protection during deserialization
as possible, at least until something better comes along. It will by no means be able to
prevent every possible attack, but hopefully it raises the bar.

As of this writing (version 0.1) the library is basically a proof-of-concept and very 
simplistic, but it works as advertised. Writing rules is cumbersome and it will need to
be easier to whitelist "normal stuff" in order to be really useful, so this is a major
priority. In addition, I'll be researching other avenues of protection in addition to
class resolution. Things that come to mind are some string verification (UTF range 
restriction) and possibly tying in with a SecurityManager to restrict certain operations.

As with most of my projects, this fills a current need. How aggressively it is maintained 
will depend on continued need, usefulness to the community and contributions from others. 
So, if you find it useful, please let me know: robeden1 at gmail. 



License
-------

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
