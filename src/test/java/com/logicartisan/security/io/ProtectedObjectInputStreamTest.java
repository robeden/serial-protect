/*
 * Copyright (C) 2015 Rob Eden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.logicartisan.security.io;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 */
@SuppressWarnings( "UnnecessaryBoxing" )
public class ProtectedObjectInputStreamTest {
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void testDeniedClass() throws Exception {
		// Setup
		ByteArrayOutputStream byte_out = new ByteArrayOutputStream();
		ObjectOutputStream out = new ObjectOutputStream( byte_out );
		out.writeObject( new Integer( 1 ) );
		out.writeObject( new Long( 2 ) );
		out.close();
		byte[] serialized = byte_out.toByteArray();

		InputPolicy policy = new InputPolicy.Builder()
			.blacklist( Long.class )
			.build();


		ByteArrayInputStream byte_in = new ByteArrayInputStream( serialized );
		ObjectInputStream in = new ProtectedObjectInputStream( policy, byte_in );
		// Reading Integer is fine
		Assert.assertEquals( Integer.valueOf( 1 ), in.readObject() );

		// Expected error when reading Long
		thrown.expect( DeniedClassException.class );
		thrown.expectMessage( "Class \"java.lang.Long\" is denied." );

		in.readObject();
	}
}
