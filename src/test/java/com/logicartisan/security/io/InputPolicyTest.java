/*
 * Copyright (C) 2015 Rob Eden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.logicartisan.security.io;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 */
public class InputPolicyTest {
	@Test
	public void emptyPolicy() {
		InputPolicy policy = new InputPolicy.Builder().build();

		assertTrue( policy.classResolveAllowed( "Foo" ) );
		assertTrue( policy.classResolveAllowed( "com.foo.Foo" ) );
	}

	@Test
	public void whitelistedClass() {
		InputPolicy policy = new InputPolicy.Builder()
			.whitelist( String.class )
			.build();

		assertTrue( policy.classResolveAllowed( "java.lang.String" ) );
		assertFalse( policy.classResolveAllowed( "com.foo.Foo" ) );
	}

	@Test
	public void whitelistedClassString() {
		InputPolicy policy = new InputPolicy.Builder()
			.whitelistClass( "java.lang.String" )
			.build();

		assertTrue( policy.classResolveAllowed( "java.lang.String" ) );
		assertFalse( policy.classResolveAllowed( "com.foo.Foo" ) );
	}

	@Test
	public void whitelistedPackage() {
		InputPolicy policy = new InputPolicy.Builder()
			.whitelistPackage( "java.lang" )
			.build();

		assertTrue( policy.classResolveAllowed( "java.lang.String" ) );
		assertFalse( policy.classResolveAllowed( "com.foo.Foo" ) );
	}


	@Test
	public void blacklistedClass() {
		InputPolicy policy = new InputPolicy.Builder()
			.blacklist( String.class )
			.build();

		assertFalse( policy.classResolveAllowed( "java.lang.String" ) );
		assertTrue( policy.classResolveAllowed( "com.foo.Foo" ) );
	}

	@Test
	public void blacklistedClassString() {
		InputPolicy policy = new InputPolicy.Builder()
			.blacklistClass( "java.lang.String" )
			.build();

		assertFalse( policy.classResolveAllowed( "java.lang.String" ) );
		assertTrue( policy.classResolveAllowed( "com.foo.Foo" ) );
	}

	@Test
	public void blacklistedPackage() {
		InputPolicy policy = new InputPolicy.Builder()
			.blacklistPackage( "java.lang" )
			.build();

		assertFalse( policy.classResolveAllowed( "java.lang.String" ) );
		assertTrue( policy.classResolveAllowed( "com.foo.Foo" ) );
	}



	@Test
	public void overriddenBlacklistedPackage() {
		InputPolicy policy = new InputPolicy.Builder()
			.blacklistPackage( "com.foo" )
			.whitelistClass( "com.foo.Foo" )
			.build();

		assertTrue( policy.classResolveAllowed( "com.foo.Foo" ) );
		assertFalse( policy.classResolveAllowed( "com.foo.Bar" ) );
	}

	@Test
	public void blacklistClassWhitelistPackage() {
		InputPolicy policy = new InputPolicy.Builder()
			.whitelistPackage( "com.foo" )
			.blacklistClass( "com.foo.Foo" )
			.build();

		assertFalse( policy.classResolveAllowed( "com.foo.Foo" ) );
		assertTrue( policy.classResolveAllowed( "com.foo.Bar" ) );
	}



	@Test
	public void defaultPolicy() {
		InputPolicy policy = new InputPolicy.Builder().build();

		assertFalse( policy.classResolveAllowed(
			"org.apache.commons.collections.functors.InvokerTransformer" ) );
	}

	@Test
	public void defaultPolicyExtension() {
		InputPolicy default_policy = new InputPolicy.Builder().build();

		InputPolicy policy = new InputPolicy.Builder( default_policy ).build();

		assertFalse( policy.classResolveAllowed(
			"org.apache.commons.collections.functors.InvokerTransformer" ) );
	}



	@Test( expected = IllegalArgumentException.class )
	public void classBlackAndWhitelist() {
		new InputPolicy.Builder()
			.blacklist( String.class )
			.whitelist( String.class );
	}

	@Test( expected = IllegalArgumentException.class )
	public void classStringBlackAndWhitelist() {
		new InputPolicy.Builder()
			.blacklistClass( "Foo" )
			.whitelistClass( "Foo" );
	}

	@Test( expected = IllegalArgumentException.class )
	public void packageStringBlackAndWhitelist() {
		new InputPolicy.Builder()
			.blacklistClass( "foo" )
			.whitelistClass( "foo" );
	}
}
