/*
 * Copyright (C) 2015 Rob Eden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.logicartisan.security.io;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static java.util.Objects.requireNonNull;

/**
 * An immutable, thread-safe (once built: builders are not thread-safe) policy controlling
 * the policy allowed for reading serialized data from a
 * {@link ProtectedObjectInputStream}.
 * <p/>
 * <b>Black/Whitelists</b>
 * Both classes and packages can be blacklisted and whitelisted. When a class is
 * encountered in the stream the determination as to whether it is allowed happens as
 * follows:
 * <ol>
 *     <li>If the class is blacklisted, the request is denied.</li>
 *     <li>If the class is whitelisted, the request is allowed.</li>
 *     <li>If the package is blacklisted, the request is denied.</li>
 *     <li>If the package is whitelisted, the request is allowed.</li>
 *     <li>If there are any whitelisted items, the request is denied.</li>
 *     <li>Otherwise, the request is allowed.</li>
 * </ol>
 */
public final class InputPolicy {
	/**
	 * The default (starting) policy which will be updated with known vulnerabilities.
	 */
	private static InputPolicy DEFAULT_POLICY;
	static {
		DEFAULT_POLICY = new InputPolicy(
			// http://foxglovesecurity.com/2015/11/06/what-do-weblogic-websphere-jboss-jenkins-opennms-and-your-application-have-in-common-this-vulnerability/
			"org.apache.commons.collections.functors.InvokerTransformer"
		);
	}


	// TODO: really basic implementation. Make this smarter, faster, better...
	private final Set<String> whitelist_classes;
	private final Set<String> whitelist_packages;
	private final Set<String> blacklist_classes;
	private final Set<String> blacklist_packages;


	private InputPolicy( Builder builder ) {
		// NOTE: Sets are only read, so safe for multiple threads, per HashSet docs
		whitelist_classes = Collections.unmodifiableSet( builder.whitelist_classes );
		whitelist_packages = Collections.unmodifiableSet( builder.whitelist_packages );
		blacklist_classes = Collections.unmodifiableSet( builder.blacklist_classes );
		blacklist_packages = Collections.unmodifiableSet( builder.blacklist_packages );
	}

	private InputPolicy( String... blacklist_class_names ) {
		blacklist_classes = Collections.unmodifiableSet( new HashSet<String>(
			Arrays.asList( blacklist_class_names ) ) );

		whitelist_classes = Collections.emptySet();
		whitelist_packages = Collections.emptySet();
		blacklist_packages = Collections.emptySet();
	}


	boolean classResolveAllowed( String class_name ) {
		requireNonNull( class_name );

		if ( blacklist_classes.contains( class_name ) ) return false;

		if ( whitelist_classes.contains( class_name ) ) return true;


		int last_dot_index = class_name.lastIndexOf( '.' );
		if ( last_dot_index > 0 ) {
			String package_name = class_name.substring( 0, last_dot_index );

			if ( blacklist_packages.contains( package_name ) ) return false;

			if ( whitelist_packages.contains( package_name ) ) return true;
		}

		if ( !whitelist_classes.isEmpty() || !whitelist_packages.isEmpty() ) {
			return false;
		}

		return true;
	}



	public static class Builder {
		private final Set<String> whitelist_classes = new HashSet<String>();
		private final Set<String> whitelist_packages = new HashSet<String>();

		private final Set<String> blacklist_classes = new HashSet<String>();
		private final Set<String> blacklist_packages = new HashSet<String>();


		/**
		 * Copies settings from an existing policy to allow further customization.
		 */
		public Builder( InputPolicy starting_policy ) {
			whitelist_classes.addAll( starting_policy.whitelist_classes );
			whitelist_packages.addAll( starting_policy.whitelist_packages );
			blacklist_classes.addAll( starting_policy.blacklist_classes );
			blacklist_packages.addAll( starting_policy.blacklist_packages );
		}

		public Builder() {
			this( DEFAULT_POLICY );
		}



		public Builder whitelist( Class c ) {
			return whitelistClass( c.getName() );
		}

		public Builder whitelistClass( String class_name ) {
			addToSet( class_name, whitelist_classes, blacklist_classes, "blacklist" );
			return this;
		}

		public Builder whitelistPackage( String package_name ) {
			addToSet( package_name, whitelist_packages, blacklist_packages, "blacklist" );
			return this;
		}
		// TODO: allow specifying whether sub-packages are matched


		public Builder blacklist( Class c ) {
			return blacklistClass( c.getName() );
		}

		public Builder blacklistClass( String class_name ) {
			addToSet( class_name, blacklist_classes, whitelist_classes, "whitelist" );
			return this;
		}

		public Builder blacklistPackage( String package_name ) {
			addToSet( package_name, blacklist_packages, whitelist_packages, "whitelist" );
			return this;
		}
		// TODO: allow specifying whether sub-packages are matched



		public InputPolicy build() {
			return new InputPolicy( this );
		}


		private void addToSet( String value, Set<String> target,
			Set<String> antithesis, String antithesis_name ) {

			requireNonNull( value );

			if ( antithesis.contains( value ) ) {
				throw new IllegalArgumentException( target + " is already in " +
					antithesis_name );
			}

			target.add( value );
		}
	}
}
