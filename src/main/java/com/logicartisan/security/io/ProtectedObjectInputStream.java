/*
 * Copyright (C) 2015 Rob Eden
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.logicartisan.security.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.util.Objects;

/**
 * An {@link ObjectInputStream} that applies a given protection policy during reading.
 * It is recommended that custom policies be used and made as restrictive as possible to
 * your application. With no customization, the default policy will provide some minimal
 * protection from known attacks, though again: this should not be relied upon and
 * tailored rules are recommended.
 */
public class ProtectedObjectInputStream extends ObjectInputStream {
	private final InputPolicy policy;

	public ProtectedObjectInputStream( InputPolicy policy, InputStream in )
		throws IOException {

		super( in );

		this.policy = Objects.requireNonNull( policy );
	}


	@Override
	protected Class<?> resolveClass( ObjectStreamClass desc )
		throws IOException, ClassNotFoundException {

		String class_name = Objects.requireNonNull( desc.getName() );

		if ( !policy.classResolveAllowed( class_name ) ) {
			throw new DeniedClassException( "Class \"" + class_name + "\" is denied." );
		}

		return super.resolveClass( desc );
	}
}
